/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONTEXT_HH_E45B6643C944437CB307F5445F6B5963
#define CONTEXT_HH_E45B6643C944437CB307F5445F6B5963

#include <initializer_list>
#include <string>

namespace LibLog {

class SetContext
{

public:
    explicit SetContext(std::initializer_list<std::string> _context_hierarchy);

    ~SetContext();

    SetContext() = delete;
    SetContext(const SetContext&) = delete;
    SetContext(SetContext&&) = delete;
    SetContext& operator=(const SetContext&) = delete;
    SetContext& operator=(SetContext&&) = delete;

private:
    std::string previous_context_;
};

const std::string& get_context();

} // namespace LibLog

#endif
