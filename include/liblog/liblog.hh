/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIBLOG_HH_24F61C58C4784C4EB1046E66360A17D8
#define LIBLOG_HH_24F61C58C4784C4EB1046E66360A17D8

// SPDLOG_NO_NAME only applies to default spdlog pattern
#define SPDLOG_NO_NAME

#if defined(FMT_STRING_ALIAS) && FMT_STRING_ALIAS
#define LIBLOG_CHECK_FORMAT(x) fmt(x)
#else
#define LIBLOG_CHECK_FORMAT(x) FMT_STRING(x)
#endif

#include "liblog/context.hh"
#include "liblog/thread_number.hh"

#include <spdlog/fmt/fmt.h>
#include <spdlog/spdlog.h>

#include <exception>
#include <string>

#include <unistd.h>

#define LIBLOG_CALL(SPDLOG, msg, ...) \
        SPDLOG("{{{}-{}{}}} {} -- {}", \
                getpid(), \
                LibLog::thread_number(), \
                LibLog::get_context(), \
                __func__, \
                LibLog::format(msg, ## __VA_ARGS__))

#define LIBLOG_TRACE(msg, ...) LIBLOG_CALL(SPDLOG_TRACE, msg, ## __VA_ARGS__)
#define LIBLOG_DEBUG(msg, ...) LIBLOG_CALL(SPDLOG_DEBUG, msg, ## __VA_ARGS__)
#define LIBLOG_INFO(msg, ...) LIBLOG_CALL(SPDLOG_INFO, msg, ## __VA_ARGS__)
#define LIBLOG_WARNING(msg, ...) LIBLOG_CALL(SPDLOG_WARN, msg, ## __VA_ARGS__)
#define LIBLOG_ERROR(msg, ...) LIBLOG_CALL(SPDLOG_ERROR, msg, ## __VA_ARGS__)
#define LIBLOG_CRITICAL(msg, ...) LIBLOG_CALL(SPDLOG_CRITICAL, msg, ## __VA_ARGS__)

#define LIBLOG_SET_CONTEXT(cls, var, ...) \
struct cls \
{ \
    LibLog::SetContext ctx; \
    cls(std::initializer_list<std::string> _context_hierarchy) : ctx{_context_hierarchy} \
    { \
        LIBLOG_DEBUG("context entered"); \
    } \
    ~cls() \
    { \
        try \
        { \
            LIBLOG_DEBUG("leaving context"); \
        } \
        catch (...) { } \
    } \
} var{__VA_ARGS__}

namespace LibLog {

template<typename Msg, typename... Args>
std::string format(const Msg& msg, const Args& ...args)
{
    try
    {
        return fmt::format(msg, args...);
    }
    catch (const std::exception&)
    {
        return "LibLog: Exception during log message formatting";
    }
    catch (...)
    {
        return "LibLog: Unknown exception during log message formatting";
    }
}

} // namespace LibLog

#endif
