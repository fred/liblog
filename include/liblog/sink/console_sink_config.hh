/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONSOLE_SINK_CONFIG_HH_5741570EAD864B81B14B3E7A30B0C439
#define CONSOLE_SINK_CONFIG_HH_5741570EAD864B81B14B3E7A30B0C439

#include "liblog/color_mode.hh"
#include "liblog/level.hh"

#include <boost/optional.hpp>

#include <string>

namespace LibLog {
namespace Sink {

struct ConsoleSinkConfig
{
    enum struct OutputStream
    {
        stdout,
        stderr
    };

    ConsoleSinkConfig();

    ConsoleSinkConfig& set_level(Level _level);

    ConsoleSinkConfig& set_output_stream(OutputStream _output_stream);

    ConsoleSinkConfig& set_color_mode(ColorMode _color_mode);

    boost::optional<Level> level;
    OutputStream output_stream;
    ColorMode color_mode;
};

} // namespace LibLog::Sink
} // namespace LibLog

#endif
