/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SYSLOG_SINK_CONFIG_HH_ECD281A6B92C4ED99B96B7AA94A3FAC3
#define SYSLOG_SINK_CONFIG_HH_ECD281A6B92C4ED99B96B7AA94A3FAC3

#include "liblog/level.hh"

#include <boost/optional.hpp>

#include <string>

namespace LibLog {
namespace Sink {

struct SyslogSinkConfig
{
    SyslogSinkConfig();

    SyslogSinkConfig& set_level(Level _level);

    SyslogSinkConfig& set_syslog_ident(const std::string& _syslog_ident);

    SyslogSinkConfig& set_syslog_options(int _syslog_options);

    SyslogSinkConfig& set_syslog_facility(int _syslog_facility);

    static std::string pattern()
    {
        return "%v -- {%s:%#}";
    }

    boost::optional<Level> level;
    std::string syslog_ident;
    int syslog_options;
    int syslog_facility;
};

} // namespace LibLog::Sink
} // namespace LibLog

#endif
