/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOG_CONFIG_HH_CE11D312C09B4ACCA55256F2740E6EB4
#define LOG_CONFIG_HH_CE11D312C09B4ACCA55256F2740E6EB4

#include "liblog/level.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include <string>
#include <vector>

namespace LibLog {

class LogConfig
{

public:

    enum class TimeType
    {
        utc,
        local
    };

    LogConfig();

    LogConfig& set_level(Level _level);

    LogConfig& set_time_type(TimeType _time_type = TimeType::utc);

    LogConfig& add_sink_config(
            const Sink::ConsoleSinkConfig& _console_sink_config);

    LogConfig& add_sink_config(
            const Sink::FileSinkConfig& _file_sink_config);

    LogConfig& add_sink_config(
            const Sink::SyslogSinkConfig& _syslog_sink_config);

    static std::string pattern();

    Level level;
    TimeType time_type;
    std::vector<Sink::ConsoleSinkConfig> console_sink_config;
    std::vector<Sink::FileSinkConfig> file_sink_config;
    std::vector<Sink::SyslogSinkConfig> syslog_sink_config;
};

} // namespace LibLog

#endif
