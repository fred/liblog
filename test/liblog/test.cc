/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "liblog/liblog.hh"
#include "liblog/log.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include "fmt/ostr.h"

#include <cstdio>
#include <thread>
#include <vector>
#include <iostream>

constexpr LibLog::ThreadMode threading = LibLog::ThreadMode::multi_threaded; // global, compile-time parameter

void somefunc()
{
    LIBLOG_SET_CONTEXT(Context, ctx, "tctx", "thread");
    LIBLOG_DEBUG("thread log somefunc foo: tid: {}", std::this_thread::get_id());
    LIBLOG_DEBUG("thread log somefunc bar: tid: {}", std::this_thread::get_id());
}

void thread_log_function()
{
    LIBLOG_SET_CONTEXT(Context, ctx, "tctx", "thread");
    LIBLOG_DEBUG("thread log foo: tid: {}", std::this_thread::get_id());
    LIBLOG_DEBUG("thread log bar: tid: {}", std::this_thread::get_id());
    somefunc();
    somefunc();
    somefunc();
}

int main()
{
    auto liblog_log_config = LibLog::LogConfig();
    liblog_log_config.add_sink_config(LibLog::Sink::ConsoleSinkConfig());
    liblog_log_config.add_sink_config(LibLog::Sink::ConsoleSinkConfig()
            .set_output_stream(LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr)
            .set_level(LibLog::Level::critical));
    liblog_log_config.add_sink_config(LibLog::Sink::FileSinkConfig("liblog-example.log")
            .set_level(LibLog::Level::debug));
    liblog_log_config.add_sink_config(LibLog::Sink::SyslogSinkConfig());
    LibLog::Log::start<threading>(liblog_log_config);

    LIBLOG_TRACE("liblog-trace-{}", 1);
    LIBLOG_DEBUG("liblog-debug-");
    LIBLOG_DEBUG("liblog-debug-{}", 1);
    LIBLOG_INFO("liblog-info-{}", 1);
    LIBLOG_WARNING("liblog-warning-{}", 1);
    LIBLOG_ERROR("liblog-error-{}", 1);
    LIBLOG_CRITICAL("liblog-critical-{}", 1);

    std::string context{};
    context = ".";
    LIBLOG_DEBUG(context);
    {
        LIBLOG_SET_CONTEXT(Context, ctx, "foo");
        context = ".foo";
        LIBLOG_DEBUG(context);
        {
            LIBLOG_SET_CONTEXT(Context, ctx2, "bar");
            context = ".foo.bar";
            LIBLOG_DEBUG(context);
            {
                LIBLOG_SET_CONTEXT(Context, ctx3, "baz");
                context = ".foo.bar.baz";
                LIBLOG_DEBUG(context);
            }
        }

        LIBLOG_SET_CONTEXT(Context2, ctx4, "quix", "quack");
        context = ".foo.quix-quack";
        LIBLOG_DEBUG(context);
    }
    context = ".";
    LIBLOG_DEBUG(context);

    {
        LIBLOG_SET_CONTEXT(Context, ctx, "foo");
        context = ".foo(again)";
        LIBLOG_DEBUG(context);
        {
            LIBLOG_SET_CONTEXT(Context, ctx2, "bar");
            context = ".foo.bar(again)";
            LIBLOG_DEBUG(context);
        }
    }

    LIBLOG_DEBUG("#main");
    LIBLOG_DEBUG("threads available: {}", std::thread::hardware_concurrency());
    std::vector<std::thread> threads(std::thread::hardware_concurrency());

    for (std::size_t thread_index = 0; thread_index < threads.size(); ++thread_index)
    {
        threads[thread_index] = std::thread(&thread_log_function);
    }

    LIBLOG_DEBUG("#main");

    threads.push_back(std::thread(&thread_log_function));

    LIBLOG_DEBUG("#main");

    for (auto& t : threads)
    {
        t.join();
    }

    LIBLOG_DEBUG("liblog-debug-indexing-{0}{1}{2}-{2}{1}{0}", 1, 2, 3);

    //LIBLOG_DEBUG("liblog-debug-error-{1}");
}
