/*
 * Copyright (C) 2019-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/liblog/sink/impl/syslog_sink.hh"

#include "src/liblog/impl/to_spdlog_level.hh"

#include <spdlog/sinks/syslog_sink.h>

namespace LibLog {
namespace Sink {
namespace Impl {

namespace {

template <ThreadMode>
struct SyslogSink;

template <>
struct SyslogSink<ThreadMode::single_threaded>
{
    using SpdlogSyslogSink = spdlog::sinks::syslog_sink_st;
};

template <>
struct SyslogSink<ThreadMode::multi_threaded>
{
    using SpdlogSyslogSink = spdlog::sinks::syslog_sink_mt;
};

template <ThreadMode thread_mode>
spdlog::sink_ptr make_shared_spdlog_sink(
        const Sink::SyslogSinkConfig& _syslog_sink_config)
{
    static constexpr auto format_message = true;

    return std::make_shared<typename SyslogSink<thread_mode>::SpdlogSyslogSink>(
            _syslog_sink_config.syslog_ident,
            _syslog_sink_config.syslog_options,
            _syslog_sink_config.syslog_facility,
            format_message);
}

} // namespace LibLog::Sink::Impl::{anonymous}

template <ThreadMode thread_mode>
void add_sink(const Sink::SyslogSinkConfig& _syslog_sink_config)
{
    auto spdlog_syslog_sink = make_shared_spdlog_sink<thread_mode>(_syslog_sink_config);
    if (_syslog_sink_config.level != boost::none)
    {
        spdlog_syslog_sink->set_level(LibLog::Impl::to_spdlog_level(_syslog_sink_config.level.value()));
    }
    spdlog::default_logger()->sinks().push_back(spdlog_syslog_sink);
    spdlog_syslog_sink->set_pattern(_syslog_sink_config.pattern());
}

template
void add_sink<ThreadMode::single_threaded>(const Sink::SyslogSinkConfig& _syslog_sink_config);

template
void add_sink<ThreadMode::multi_threaded>(const Sink::SyslogSinkConfig& _syslog_sink_config);

} // namespace LibLog::Sink::Impl
} // namespace LibLog::Sink
} // namespace LibLog
