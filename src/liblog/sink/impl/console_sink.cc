/*
 * Copyright (C) 2019-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/liblog/sink/impl/console_sink.hh"

#include "liblog/color_mode.hh"

#include "src/liblog/impl/to_spdlog_level.hh"

#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace LibLog {
namespace Sink {
namespace Impl {

namespace {

spdlog::color_mode to_spdlog_color_mode(ColorMode _color_mode)
{
    switch (_color_mode)
    {
        case ColorMode::never:
            return spdlog::color_mode::never;
        case ColorMode::automatic:
            return spdlog::color_mode::automatic;
        case ColorMode::always:
            return spdlog::color_mode::always;
    }
    throw std::logic_error{"invalid LibLog::Sink::ColorSinkConfig::ColorMode"};
}

template <ThreadMode>
struct ConsoleSink;

template <>
struct ConsoleSink<ThreadMode::single_threaded>
{
    using SpdlogStdoutSink = spdlog::sinks::stdout_sink_st;
    using SpdlogStderrSink = spdlog::sinks::stderr_sink_st;
    using SpdlogStdoutColorSink = spdlog::sinks::stdout_color_sink_st;
    using SpdlogStderrColorSink = spdlog::sinks::stderr_color_sink_st;
};

template <>
struct ConsoleSink<ThreadMode::multi_threaded>
{
    using SpdlogStdoutSink = spdlog::sinks::stdout_sink_mt;
    using SpdlogStderrSink = spdlog::sinks::stderr_sink_mt;
    using SpdlogStdoutColorSink = spdlog::sinks::stdout_color_sink_mt;
    using SpdlogStderrColorSink = spdlog::sinks::stderr_color_sink_mt;
};

template <ThreadMode thread_mode>
spdlog::sink_ptr make_shared_spdlog_sink(
        const Sink::ConsoleSinkConfig& _console_sink_config)
{
    switch (_console_sink_config.output_stream)
    {
        case Sink::ConsoleSinkConfig::OutputStream::stdout:
            switch (_console_sink_config.color_mode)
            {
                case ColorMode::never:
                    return std::make_shared<typename ConsoleSink<thread_mode>::SpdlogStdoutSink>();
                case ColorMode::automatic:
                case ColorMode::always:
                    return std::make_shared<typename ConsoleSink<thread_mode>::SpdlogStdoutColorSink>(to_spdlog_color_mode(_console_sink_config.color_mode));
            }
            throw std::logic_error{"invalid LibLog::ColorMode"};
        case Sink::ConsoleSinkConfig::OutputStream::stderr:
            switch (_console_sink_config.color_mode)
            {
                case ColorMode::never:
                    return std::make_shared<typename ConsoleSink<thread_mode>::SpdlogStderrSink>();
                case ColorMode::automatic:
                case ColorMode::always:
                    return std::make_shared<typename ConsoleSink<thread_mode>::SpdlogStderrColorSink>(to_spdlog_color_mode(_console_sink_config.color_mode));
            }
            throw std::logic_error{"invalid LibLog::ColorMode"};
    }
    throw std::logic_error{"invalid LibLog::Sink::ConsoleSinkConfig::OutputStream"};
}

} // namespace LibLog::Sink::Impl::{anonymous}

template <ThreadMode thread_mode>
void add_sink(const Sink::ConsoleSinkConfig& _console_sink_config)
{
    auto spdlog_console_sink = make_shared_spdlog_sink<thread_mode>(_console_sink_config);
    if (_console_sink_config.level != boost::none)
    {
        spdlog_console_sink->set_level(LibLog::Impl::to_spdlog_level(_console_sink_config.level.value()));
    }
    spdlog::default_logger()->sinks().push_back(spdlog_console_sink);
}

template
void add_sink<ThreadMode::single_threaded>(const Sink::ConsoleSinkConfig& _console_sink_config);

template
void add_sink<ThreadMode::multi_threaded>(const Sink::ConsoleSinkConfig& _console_sink_config);

} // namespace LibLog::Sink::Impl
} // namespace LibLog::Sink
} // namespace LibLog
