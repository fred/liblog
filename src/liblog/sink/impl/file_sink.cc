/*
 * Copyright (C) 2019-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/liblog/sink/impl/file_sink.hh"

#include "src/liblog/impl/to_spdlog_level.hh"

#include <spdlog/sinks/basic_file_sink.h>

namespace LibLog {
namespace Sink {
namespace Impl {

namespace {

template <ThreadMode>
struct FileSink;

template <>
struct FileSink<ThreadMode::single_threaded>
{
    using SpdlogBasicFileSink = spdlog::sinks::basic_file_sink_st;
};

template <>
struct FileSink<ThreadMode::multi_threaded>
{
    using SpdlogBasicFileSink = spdlog::sinks::basic_file_sink_mt;
};

template <ThreadMode thread_mode>
spdlog::sink_ptr make_shared_spdlog_sink(
        const Sink::FileSinkConfig& _file_sink_config)
{
    return std::make_shared<typename FileSink<thread_mode>::SpdlogBasicFileSink>(_file_sink_config.filename);
}

} // namespace LibLog::Sink::Impl::{anonymous}

template <ThreadMode thread_mode>
void add_sink(const Sink::FileSinkConfig& _file_sink_config)
{
    auto spdlog_file_sink = make_shared_spdlog_sink<thread_mode>(_file_sink_config);
    if (_file_sink_config.level != boost::none)
    {
        spdlog_file_sink->set_level(LibLog::Impl::to_spdlog_level(_file_sink_config.level.value()));
    }
    spdlog::default_logger()->sinks().push_back(spdlog_file_sink);
}

template
void add_sink<ThreadMode::single_threaded>(const Sink::FileSinkConfig& _file_sink_config);

template
void add_sink<ThreadMode::multi_threaded>(const Sink::FileSinkConfig& _file_sink_config);

} // namespace LibLog::Sink::Impl
} // namespace LibLog::Sink
} // namespace LibLog
