/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONSOLE_SINK_HH_9A7F01EBF7084231AD9B3F23B62B6BA0
#define CONSOLE_SINK_HH_9A7F01EBF7084231AD9B3F23B62B6BA0

#include "liblog/sink/console_sink_config.hh"
#include "liblog/thread_mode.hh"

namespace LibLog {
namespace Sink {
namespace Impl {

template <ThreadMode thread_mode>
void add_sink(const Sink::ConsoleSinkConfig& _console_sink_config);

extern template
void add_sink<ThreadMode::single_threaded>(const Sink::ConsoleSinkConfig& _console_sink_config);

extern template
void add_sink<ThreadMode::multi_threaded>(const Sink::ConsoleSinkConfig& _console_sink_config);
} // namespace LibLog::Sink::Impl
} // namespace LibLog::Sink
} // namespace LibLog

#endif
