/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "liblog/sink/syslog_sink_config.hh"

#include <syslog.h>

namespace LibLog {
namespace Sink {

SyslogSinkConfig::SyslogSinkConfig()
    : syslog_ident{""},
      syslog_options{LOG_ODELAY},
      syslog_facility{LOG_USER}
{
}

SyslogSinkConfig& SyslogSinkConfig::set_level(Level _level)
{
    level = _level;
    return *this;
}

SyslogSinkConfig& SyslogSinkConfig::set_syslog_ident(const std::string& _syslog_ident)
{
    syslog_ident = _syslog_ident;
    return *this;
}

SyslogSinkConfig& SyslogSinkConfig::set_syslog_options(const int _syslog_options)
{
    syslog_options = _syslog_options;
    return *this;
}

SyslogSinkConfig& SyslogSinkConfig::set_syslog_facility(const int _syslog_facility)
{
    syslog_facility = _syslog_facility;
    return *this;
}

} // namespace LibLog::Sink
} // namespace LibLog
