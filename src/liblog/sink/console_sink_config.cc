/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "liblog/sink/console_sink_config.hh"

#include <string>

namespace LibLog {
namespace Sink {

ConsoleSinkConfig::ConsoleSinkConfig()
    : output_stream{OutputStream::stdout},
      color_mode{ColorMode::automatic}
{
}

ConsoleSinkConfig& ConsoleSinkConfig::set_level(Level _level)
{
    level = _level;
    return *this;
}

ConsoleSinkConfig& ConsoleSinkConfig::set_output_stream(OutputStream _output_stream)
{
    output_stream = _output_stream;
    return *this;
}

ConsoleSinkConfig& ConsoleSinkConfig::set_color_mode(ColorMode _color_mode)
{
    color_mode = _color_mode;
    return *this;
}

} // namespace LibLog::Sink
} // namespace LibLog
