/*
 * Copyright (C) 2019-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "liblog/log.hh"

#include "src/liblog/sink/impl/console_sink.hh"
#include "src/liblog/sink/impl/file_sink.hh"
#include "src/liblog/sink/impl/syslog_sink.hh"

#include "src/liblog/impl/to_spdlog_level.hh"
#include "src/liblog/impl/to_spdlog_pattern_time_type.hh"

#include <spdlog/async_logger.h>
#include <spdlog/logger.h>

namespace LibLog {

Log::Log(const LogConfig& _log_config)
    : config{_log_config}
{
}

template<ThreadMode thread_mode>
void Log::start(const LogConfig& _log_config)
{
    LibLog::Log log{_log_config};

    spdlog::set_default_logger(std::make_shared<spdlog::logger>(""));
    spdlog::set_level(Impl::to_spdlog_level(log.config.level));
    spdlog::default_logger_raw()->flush_on(spdlog::level::trace);
    for (const auto& console_sink_config : _log_config.console_sink_config)
    {
        LibLog::Sink::Impl::add_sink<thread_mode>(console_sink_config);
    }
    for (const auto& file_sink_config : _log_config.file_sink_config)
    {
        LibLog::Sink::Impl::add_sink<thread_mode>(file_sink_config);
    }
    spdlog::set_pattern(_log_config.pattern(), Impl::to_spdlog_pattern_time_type(_log_config.time_type));
    for (const auto& syslog_sink_config :_log_config.syslog_sink_config)
    {
        LibLog::Sink::Impl::add_sink<thread_mode>(syslog_sink_config);
    }
}

template void Log::start<ThreadMode::single_threaded>(const LogConfig& _log_config);

template void Log::start<ThreadMode::multi_threaded>(const LogConfig& _log_config);

} // namespace LibLog
