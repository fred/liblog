/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "liblog/context.hh"

namespace LibLog {

namespace {

int call_id(bool _next)
{
    thread_local int call_id = 0;
    return _next ? ++call_id : call_id;
}

std::string& context()
{
    thread_local std::string context;
    return context;
}

void set_context(std::string _context)
{
    context() = std::move(_context);
}

std::string make_context_str(const std::initializer_list<std::string> _context_hierarchy)
{
    std::string result = "";
    static const std::string delimiter = "-";
    const auto get_next_call_id = context().empty();
    result += delimiter + std::to_string(call_id(get_next_call_id));
    for (const auto& context_element : _context_hierarchy)
    {
        result += delimiter + context_element;
    }
    return result;
}

} // namespace LibLog::{anonymous}

SetContext::SetContext(const std::initializer_list<std::string> _context_hierarchy)
    : previous_context_{get_context()}
{
    set_context(make_context_str(_context_hierarchy));
}

SetContext::~SetContext()
{
    set_context(std::move(previous_context_));
}

const std::string& get_context()
{
    return context();
}

} // namespace LibLog
