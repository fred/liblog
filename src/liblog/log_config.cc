/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "liblog/log_config.hh"

namespace LibLog {

LogConfig::LogConfig()
    : level{Level::trace},
      time_type{TimeType::utc}
{
}

LogConfig& LogConfig::set_level(Level _level)
{
    level = _level;
    return *this;
}

LogConfig& LogConfig::set_time_type(TimeType _time_type)
{
    time_type = _time_type;
    return *this;
}

LogConfig& LogConfig::add_sink_config(
        const Sink::ConsoleSinkConfig& _console_sink_config)
{
    console_sink_config.push_back(_console_sink_config);
    return *this;
}

LogConfig& LogConfig::add_sink_config(
        const Sink::FileSinkConfig& _file_sink_config)
{
    file_sink_config.push_back(_file_sink_config);
    return *this;
}

LogConfig& LogConfig::add_sink_config(
        const Sink::SyslogSinkConfig& _syslog_sink_config)
{
    syslog_sink_config.push_back(_syslog_sink_config);
    return *this;
}

std::string LogConfig::pattern()
{
    return "%Y-%m-%dT%H:%M:%S.%f <%^%l%$> %v -- {%s:%#}";
}

} // namespace LibLog
