/*
 * Copyright (C) 2019  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TO_SPDLOG_PATTERN_TIME_TYPE_HH_F606F66B286A4A7EAB278A1F8A48DD01
#define TO_SPDLOG_PATTERN_TIME_TYPE_HH_F606F66B286A4A7EAB278A1F8A48DD01

#include "liblog/log_config.hh"

#include "3rd_party/lib/spdlog/include/spdlog/common.h"

namespace LibLog {
namespace Impl {

spdlog::pattern_time_type to_spdlog_pattern_time_type(LogConfig::TimeType _time_type);

} // namespace LibLog::Impl
} // namespace LibLog

#endif
