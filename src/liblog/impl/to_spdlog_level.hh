/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TO_SPDLOG_LEVEL_HH_60C652CA688F4DC1A476FA23297430FB
#define TO_SPDLOG_LEVEL_HH_60C652CA688F4DC1A476FA23297430FB

#include "liblog/level.hh"

#include <spdlog/spdlog.h>

namespace LibLog {
namespace Impl {

spdlog::level::level_enum to_spdlog_level(Level _level);

} // namespace LibLog::Impl
} // namespace LibLog

#endif
