/*
 * Copyright (C) 2019-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/liblog/impl/to_spdlog_pattern_time_type.hh"

#include <stdexcept>

namespace LibLog {
namespace Impl {

spdlog::pattern_time_type to_spdlog_pattern_time_type(LogConfig::TimeType _time_type)
{
    switch (_time_type)
    {
        case LogConfig::TimeType::utc:
            return spdlog::pattern_time_type::utc;
        case LogConfig::TimeType::local:
            return spdlog::pattern_time_type::local;
    }
    throw std::logic_error{"invalid LibLog::LogConfig::TimeType"};
}

} // namespace LibLog::Impl
} // namespace LibLog
