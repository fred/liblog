/*
 * Copyright (C) 2019-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/liblog/impl/to_spdlog_level.hh"

namespace LibLog {
namespace Impl {

spdlog::level::level_enum to_spdlog_level(Level _level)
{
    switch (_level)
    {
        case Level::trace: return spdlog::level::trace;
        case Level::debug: return spdlog::level::debug;
        case Level::info: return spdlog::level::info;
        case Level::warning: return spdlog::level::warn;
        case Level::error: return spdlog::level::err;
        case Level::critical: return spdlog::level::critical;
    }
    throw std::logic_error{"invalid LibLog::Level"};
}

} // namespace LibLog::Impl
} // namespace LibLog
