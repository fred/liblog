ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

1.4.2 (2022-02-02)
------------------

* Freeze spdlog on version ``v1.9.2``

1.4.1 (2022-01-04)
------------------

* Reduce build time (do not use ``fmt`` library as header-only by default)

1.4.0 (2021-12-17)
------------------

* Update CI
* Update CMake build
* Reduce number of include directories

1.3.0 (2021-02-19)
------------------

* CI fixes
* Fix build

  * tarball name
  * distcheck

* Add bumpversion config

1.2.0 (2020-12-10)
------------------

* Increment call-id only if changing root context (simplify message grouping for one *call* - defined by top level context)
* Rename changelog to CHANGELOG.rst to match all FRED projects

1.1.1 (2020-09-02)
------------------

* Fix messages flushing


1.1.0 (2020-07-10)
------------------

* Add autoincrement call-id (on context entry)


1.0.0 (2020-04-20)
------------------

* Initial release
